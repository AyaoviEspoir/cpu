/**
 * Implementation of InterruptHandler interface
 * 
 * @author Ayaovi Espoir Djissenou
 * @version 20/04/2016
 */

public class InterruptHandlerImpl implements InterruptHandler
{

	public InterruptHandlerImpl() {}
	/**
     * Invoke the interrupt handler, providing the interrupt type and zero or more arguments.
     */
    public void interrupt(int interruptType, Object... varargs)
    {
    	switch(interruptType)
    	{
    		case TIME_OUT:
    		{
    			// we probably want to context switch.
                /*ProcessControlBlock pcb = (ProcessControlBlock)varargs[0];
                pcb.setState(State.READY);
                Config.getKernel().addToReadyQueue(pcb);
                
                // if(!readyQueue.isEmpty())
                //     Config.getCPU().contextSwitch(readyQueue.remove());
                // else
                //     Config.getCPU().contextSwitch(pcb);
                Config.getCPU().contextSwitch(readyQueue.remove());*/

                Config.getKernel().interrupt(InterruptHandler.TIME_OUT, varargs);
    		}
    		break;
    		case WAKE_UP:
    		{
    			// we want to move the process from WAITING queue to READY queue
    			// if the CPU is busy. Otherwise put the process back onto the CPU.
    			/*ProcessControlBlock pcb = (ProcessControlBlock)varargs[1];
    			assert(pcb.getState() == State.WAITING);
                pcb.setState(State.READY);
    			Config.getKernel().addToReadyQueue(pcb);
    			Config.getDevice((Integer)varargs[0]).remove(pcb.getPID());*/

                Config.getKernel().interrupt(InterruptHandler.WAKE_UP, varargs);
    		}
    		break;
    		default:
    	}
    }
}
/**
 * FileReader.java - reads the content of a file
 * @author Ayaovi Espoir Djissenou
 * @version 30/03/2016
 */

import java.util.ArrayList;
import java.util.Arrays;
//
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

class CCFileReader 
{   
	private static ArrayList<String> data;
	private static String fileName;
	private static String exception;
  	private static int numberOfLines;

	public CCFileReader(String fileName, String exception) 
	{
		this.fileName = fileName;
		this.exception = exception;
		data = new ArrayList<String>();
		numberOfLines = 0;
		fillArray();
	}
   
	private void fillArray() 
	{
		BufferedReader br = null;
		String line = "";
		try 
		{
			br = new BufferedReader( new FileReader(fileName));
			
			while((line = br.readLine()) != null) 
			{
				//System.out.println(line);

				if (!(line.charAt(0) + "").equals(exception))
				{
					String temp[] = line.split(" ");
					data.addAll(Arrays.asList(temp));
					numberOfLines++;				
				}
			}
			//System.out.println("Size of data : " + data.size());
		}
		catch (FileNotFoundException e) 
		{
			System.err.println("Unable to find the file: "+fileName);
		}
		catch (IOException e) 
		{
			System.err.println("Unable to read the file: "+fileName);
		}
	}
   
	public static ArrayList<String> getStringArrayList() 
	{
		return data;
	}
   
	public static String[] getStringArray() 
	{
		Utilities util = new Utilities();
		return util.toStringArray(data);     
	}

	public static int[] getIntegerArray()
	{
		int integerArray[] = new int[data.size()];

		for (int i = 0; i < data.size(); i++)
		{
			integerArray[i] = Integer.parseInt(data.get(i));
		}

		return integerArray;
	}

	public static int[] getRangeOfElements(int start, int stop)
	{
		assert(start <= stop);
		int integerArray[] = new int[ numberOfLines * (stop - start) ];
		//System.out.println("Size of data : " + data.size());

		for (int i = 0; i < numberOfLines; i++)
		{
			for (int j = start; j < stop; j++)
			{
				integerArray[ i * (stop - start) + (j - start) ] = Integer.parseInt( data.get( i * numberOfLines + j ) );
			}
		}

		return integerArray;
	}

	public int getNumberOfLines()
	{
		return numberOfLines;
	}

	public void setFilename(String newFilename)
	{
		fileName = newFilename;
		data.clear();
		numberOfLines = 0;
		fillArray();
	}

	// public static int[] getSpecificElements(int start, int stop)
	// {
	// 	int integerArray[] = new int[data.size()];

	// 	for (int i = 0; i < numberOfLines; i++)
	// 	{
	// 		for (int j = start; j < stop; j++)
	// 		{
	// 			integerArray[i] = Integer.parseInt(data.get(i*numberOfLines + j));
	// 		}
	// 	}
	// }	
   
	//private static String[] toStringArray(ArrayList<String> a) {
		//int size = a.size();
		//String arr[] = new String[size];
		//for (int i=0;i<size;i++){
			//arr[i] = a.get(i);
		//}
		//return arr;
	//}
}

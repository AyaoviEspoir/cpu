/**
 * Driver file for FCFS scheduling algorithm.
 * 
 * @author Ayaovi Espoir Djissenou
 * @version 19/04/2016
 */

import java.util.Scanner;

public class SimulateFCFS
{
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);
		System.out.println("*** FCFS Simulator ***");
		
		String configFilename = "../../tests/Test2C/config.cfg";
		int sysCallCost = 1;
		int cSwitchCost = 3;
		int traceLevel = 31;

		// System.out.print("Enter configuration file name: ");
		// String configFilename = input.next();
		// System.out.println();
		
		// System.out.print("Enter cost of system call: ");
		// int sysCallCost = input.nextInt();
		// System.out.println();
		
		// System.out.print("Enter cost of context switch: ");
		// int cSwitchCost = input.nextInt();
		// System.out.println();
		
		// System.out.print("Enter trace level: ");
		// int traceLevel = input.nextInt();
		// System.out.println();

		TRACE.SET_TRACE_LEVEL(traceLevel);
		final KernelFCFS kernel = new KernelFCFS();
		Config.init(kernel, cSwitchCost, sysCallCost);
		Config.buildConfiguration(configFilename);
		Config.run();

		SystemTimer timer = Config.getSystemTimer();
		System.out.println("*** Results ***");

		System.out.println(timer);
		System.out.println("Context switches:"+Config.getCPU().getContextSwitches());
		System.out.printf("CPU utilization: %.2f\n",((double)timer.getUserTime())/timer.getSystemTime()*100);
	}
}
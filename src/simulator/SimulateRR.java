/**
 * Driver file for Round Robin scheduling algorithm.
 * 
 * @author Ayaovi Espoir Djissenou
 * @version 21/04/2016
 */

public class SimulateRR
{
	public static void main(String args[])
	{
		TRACE.SET_TRACE_LEVEL(31);
		final KernelRR kernel = new KernelRR(500);
		// Config config = new Config();
		Config.init(kernel,3,1);
		Config.buildConfiguration("../../tests/Test2B/config.cfg");
		Config.run();
		SystemTimer timer = Config.getSystemTimer();
		System.out.println(timer);
		System.out.println("Context switches:"+Config.getCPU().getContextSwitches());
		System.out.printf("CPU utilization: %.2f\n",((double)timer.getUserTime())/timer.getSystemTime()*100);
	}
}
/**
 * Implementation file based on the ProcessControlBlock interface.
 * 
 * @author Ayaovi Espoir Djissenou
 * @version 18/04/2016
 */

// import java.io.FileNotFoundException;
import java.io.IOException;
// import java.util.ArrayDeque;
import java.util.ArrayList;
// import java.util.Deque;
// 
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
//
import java.util.Scanner;

public class ProcessControlBlockImpl implements ProcessControlBlock
{
	private int processID;	
	private String processName;
	private int priority;
	private State state;
	private int currentInstructionIndex;
	private ArrayList<Instruction> instructions;

	public ProcessControlBlockImpl(ArrayList<Instruction> instructions)
	{
		priority = 0;
		currentInstructionIndex = 0;
		state = State.READY;
		this.instructions = instructions;
	}

	public ProcessControlBlockImpl(ArrayList<Instruction> instructions, String processName)
	{
		priority = 0;
		this.processName = processName;
		currentInstructionIndex = 0;
		state = State.READY;
		this.instructions = instructions;
	}

	public static ProcessControlBlock loadProgram(String filename) throws IOException //FileNotFoundException
	{
        // System.out.println("======INSIDE ProcessControlBlockImpl.loadProgram("+filename+")======");
		// try {
        final BufferedReader reader = new BufferedReader(new FileReader(filename));
        
        String line = reader.readLine().trim();

        ArrayList<Instruction> instrs = new ArrayList<Instruction>();

        while (line!=null) {
            if (line.startsWith("#") || line.equals("")) {
                // It's a commment or blank line, ignore.
            }
            else if (line.startsWith("CPU")) {
                Scanner scanner = new Scanner(line);
                scanner.next(); // Consume "CPU" indicator
                if (!scanner.hasNextInt()) {
                    // Whoops, missing program start time.
                    System.out.println("CPU instruction entry missing duration: \""+line+"\".");
                    System.exit(-1);
                }
                final int duration = scanner.nextInt();
                instrs.add(new CPUInstruction(duration));	// FIFO scheme.
            }
            else if (line.startsWith("IO")) {
                Scanner scanner = new Scanner(line);
                scanner.next(); // Consume "IO" indicator
                if (!scanner.hasNextInt()) {
                    // Whoops, missing instruction duration
                    System.out.println("IO instruction entry missing duration: \""+line+"\".");
                    System.exit(-1);
                }
                final int duration = scanner.nextInt();
                if (!scanner.hasNext()) {
                    // Whoops, missing device ID
                    System.out.println("IO instruction entry missing device ID: \""+line+"\".");
                    System.exit(-1);
                }
                final int deviceID = scanner.nextInt();
                instrs.add(new IOInstruction(duration, deviceID));	// FIFO scheme.
            }
            else {
                System.out.println("Unrecognised token in configuration file : \""+filename+"\".");
                reader.close();
                System.exit(-1);
            }
            line = reader.readLine();
        }	// end while

        reader.close();
        // System.out.println("======LEAVING ProcessControlBlockImpl.loadProgram("+filename+")======");
		return new ProcessControlBlockImpl(instrs, filename.substring(0, filename.length() - 4));
	}

	/**
     * Obtain process ID.
     */
    public int getPID()
    {
    	return processID;
    }

    /**
     * Obtain program name.
     * 
     */
    public String getProgramName()
    {
    	return processName;
    }

    /**
     * Obtain process priority();
     */
    public int getPriority()
    {
    	return priority;
    }

    /**
     * Set process priority(), returning the old value.
     */
    public int setPriority(int value)
    {
    	int oldPriority = priority;
    	priority = value;
    	return priority;
    }

	/**
     * Set process ID().
     */
    public void setID(int newID)
    {
    	processID = newID;
    }    

    /**
     * Obtain current program 'instruction'.
     */
    public Instruction getInstruction()
    {
    	// return instructions.remove();
    	return instructions.get(currentInstructionIndex);
    }

    /**
     * Determine if there are any more instructions.
     */
    public boolean hasNextInstruction()
    {
    	// return instructions.isEmpty();
    	return (currentInstructionIndex + 1) < instructions.size();
    }

    /**
     * Advance to next instruction.
     */
    public void nextInstruction()
    {
    	++currentInstructionIndex;
    }

    /**
     * Obtain process state.
     */
    public State getState()
    {
    	return state;
    }

    /**
     * Set process state.
     * Requires <code>getState()!=State.TERMINATED</code>.
     */
    public void setState(State state)
    {
    	assert(getState()!=State.TERMINATED);
    	this.state = state;
    }

    public int getNumberOfInstructions() { return instructions.size(); }

    /**
     * Obtain a String representation of the PCB of the form '{pid(&lt;pid&gt;), state(&lt;state&gt;), name(&lt;program name&gt;)}'.
     */
    public String toString()
    {
    	// return "{"+getPID()+", "+getState()+", "+getProgramName()+"}";

        return String.format("process(pid=%d, state=%s, name=\"%s\")", this.getPID(), this.getState(), this.getProgramName());
    }
}
/**
 * Implementation file of Round Robin kernel.
 * 
 * @author Ayaovi Espoir Djissenou
 * @version 20/04/2016
 */

import java.io.FileNotFoundException;
import java.io.IOException;
//
import java.util.ArrayDeque;
import java.util.Deque;

public class KernelRR implements Kernel
{
	private Deque<ProcessControlBlock> readyQueue;
    private static int processIDs;
    private final int timeSlice;
        
    public KernelRR(int timeSlice) {
		// Set up the ready queue.
        processIDs = 0;
        this.timeSlice = timeSlice;
        readyQueue = new ArrayDeque<ProcessControlBlock>();
    }

    public int getTimeSlice() { return timeSlice; }
    
    public void addToReadyQueue(ProcessControlBlock process)
    {
        readyQueue.add(process);
        if (Config.getCPU().isIdle())
        {
            dispatch();
            // Config.getCPU().
        }
    }

    private ProcessControlBlock dispatch() {
        // System.out.println("======INSIDE KernelFCFS.dispatch()======");
		// Perform context switch, swapping process
		// currently on CPU with one at front of ready queue.
		// If ready queue empty then CPU goes idle ( holds a null value).
		// Returns process removed from CPU.
        if (!readyQueue.isEmpty())
        {
            // System.out.println("======LEAVING KernelFCFS.dispatch()======");
            return Config.getCPU().contextSwitch(readyQueue.remove());
        }
        else
        {
            // System.out.println("======LEAVING KernelFCFS.dispatch()======");
            return Config.getCPU().contextSwitch(null);
        }
        // return null;
	}
                
    public int syscall(int number, Object... varargs) {
        // System.out.println("======INSIDE KernelFCFS.syscall("+number+")======");
        int result = 0;
        switch (number) {
             case MAKE_DEVICE:
                {
                    IODevice device = new IODevice((Integer)varargs[0], (String)varargs[1]);
                    Config.addDevice(device);
                }
                break;
             case EXECVE:
                {
                    ProcessControlBlock pcb = this.loadProgram((String)varargs[0]);
                    if (pcb!=null) {
                        // Loaded successfully.
						// Now add to end of ready queue.
						// If CPU idle then call dispatch.
                        // System.out.println("====PCB "+pcb.getProgramName()+" has "+pcb.getNumberOfInstructions()+" instructions====");
                        pcb.setPriority((int)varargs[1]);    // enforce priority.
                        pcb.setID(++processIDs);
                        
                        if (readyQueue == null)
                        {
                            readyQueue = new ArrayDeque<ProcessControlBlock>();
                        }

                        readyQueue.add(pcb);

                        if (Config.getCPU().isIdle())
                        {
                            ProcessControlBlock pcb2  = this.dispatch();

                            if (pcb2 != null)
                            {
                                readyQueue.add(pcb2);
                            }
<<<<<<< HEAD
                            Config.getCPU().execute(timeSlice);
=======
                            Config.getCPU().execute(timeSlice); // look at this again.
>>>>>>> implementation
                        }
                    }
                    else {
                        result = -1;
                    }
                }
                break;
             case IO_REQUEST: 
                {
					// IO request has come from process currently on the CPU.
					// Get PCB from CPU.
                    // ProcessControlBlock pcb = Config.getCPU().getCurrentProcess();
                    // System.out.println("RECEIVED IO_REQUEST");
                    ProcessControlBlock pcb = Config.getCPU().getCurrentProcess();
					
                    // Find IODevice with given ID: Config.getDevice((Integer)varargs[0]);
                    IODevice device = Config.getDevice((Integer)varargs[0]);
					// Make IO request on device providing burst time (varages[1]),
					// the PCB of the requesting process, and a reference to this kernel (so // that the IODevice can call interrupt() when the request is completed.
					device.requestIO((Integer)varargs[1], pcb, new InterruptHandlerImpl());
					// Set the PCB state of the requesting process to WAITING.
                    pcb.setState(State.WAITING);

                    if(!readyQueue.isEmpty())
                        Config.getCPU().contextSwitch(readyQueue.remove());
                    else
                        Config.getCPU().contextSwitch(null);

					// Call dispatch().
                    // dispatch();
                }
                break;
             case TERMINATE_PROCESS:
                {
					// Process on the CPU has terminated.
					// Get PCB from CPU.
                    ProcessControlBlock pcb = Config.getCPU().getCurrentProcess();
					// Set status to TERMINATED.
                    pcb.setState(State.TERMINATED);
                    // Call dispatch().
                    this.dispatch();
                }
                break;
             default:
                result = -1;
        }
        // System.out.println("======LEAVING KernelFCFS.syscall("+number+")======");
        return result;
    }
   
    
    public void interrupt(int interruptType, Object... varargs){
        switch (interruptType) {
            case TIME_OUT:
                // throw new IllegalArgumentException("FCFSKernel:interrupt("+interruptType+"...): this kernel does not suppor timeouts.");
            	{
            		// we want to context switch.
                    Integer processID = (Integer)varargs[0];
            		ProcessControlBlock pcb = Config.getCPU().getCurrentProcess();
                    assert(pcb.getPID()==processID.intValue());
            		pcb.setState(State.READY);
                    readyQueue.add(pcb);
					
                    // if(!readyQueue.isEmpty())
                    //     Config.getCPU().contextSwitch(readyQueue.remove());
                    // else
                    //     Config.getCPU().contextSwitch(pcb);
                    Config.getCPU().contextSwitch(readyQueue.remove());
            	}
            	break;

            case WAKE_UP:
				{
                    // IODevice has finished an IO request for a process.
                    // Retrieve the PCB of the process (varargs[1]), set its state
                    // to READY, put it on the end of the ready queue.
                    // If CPU is idle then dispatch().
                    ProcessControlBlock pcb = (ProcessControlBlock)varargs[1];
                    assert(pcb.getState() == State.WAITING);
                    pcb.setState(State.READY);
                    readyQueue.add(pcb);
                    // Config.getKernel().addToReadyQueue(pcb);
                    Config.getDevice((Integer)varargs[0]).remove(pcb.getPID());
                }
                break;
            default:
                throw new IllegalArgumentException("FCFSKernel:interrupt("+interruptType+"...): unknown type.");
        }
    }
    
    private static ProcessControlBlock loadProgram(String filename) {
        try {
            return ProcessControlBlockImpl.loadProgram(filename);
        }
        catch (FileNotFoundException fileExp) {
            return null;
        }
        catch (IOException ioExp) {
            return null;
        }
    }
}
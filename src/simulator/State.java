/**
 * State is an enumeration of the different states of a process
 * during its lifecycle.
 *
 * @author Ayaovi Espoir Djissenou
 * @version 20/04/2016
 */
public enum State {
	WAITING, READY, RUNNING, TERMINATED;
}